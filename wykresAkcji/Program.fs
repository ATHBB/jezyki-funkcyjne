﻿// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.
open System
open FSharp.Data
open System.Drawing
open System.Windows.Forms
open System.Windows.Forms.DataVisualization.Charting

//Odczyt csv
let msft = CsvFile.Load("http://ichart.finance.yahoo.com/table.csv?s=MSFT").Cache()

// Create an instance of chart control and add main area
let chart = new Chart(Dock = DockStyle.Fill)
let area = new ChartArea("Main")
chart.ChartAreas.Add(area)

// Create series and add it to the chart
let seriesColumns = new Series("RandomColumns")
chart.Series.Add(seriesColumns)
3


for row in msft.Rows do
  seriesColumns.Points.Add(float(row.GetColumn "Close")) |> ignore


// Show the chart control on a top-most form
let mainForm = new Form(Visible = true, TopMost = true, 
                        Width = 1250, Height = 1060)
let btn = new Button(Text = "Change Background color")
mainForm.Controls.Add(btn)
mainForm.Controls.Add(chart)
// Register event handler for button click event
btn.Click.Add(fun _ ->
  // Generate random color and set it as background
  let rnd = new Random()
  let r, g, b = rnd.Next(256), rnd.Next(256), rnd.Next(256)
  chart.BackColor <- Color.FromArgb(r, g, b) )
mainForm.Show()
Application.Run(mainForm)

[<EntryPoint>]
let main argv = 
    let w = Console.ReadKey true
    0 // return an integer exit code
